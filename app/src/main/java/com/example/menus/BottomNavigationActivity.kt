package com.example.menus

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_bottom_navigation_view.*

class BottomNavigationActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation_view)
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
            R.id.navigation_dashboard -> Toast.makeText(this, "Dashboard", Toast.LENGTH_SHORT).show()
            R.id.navigation_notifications -> Toast.makeText(this, "Notifications", Toast.LENGTH_SHORT).show()
            else -> return false
        }
        return true
    }
}

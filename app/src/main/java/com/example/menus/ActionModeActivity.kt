package com.example.menus

import android.os.Bundle
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_action_mode.*

class ActionModeActivity : AppCompatActivity() {

    private var actionMode: ActionMode? = null

    private val mActionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
            mode.menuInflater.inflate(R.menu.menu_main, menu)
            return true
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.action_settings -> {
                    // Do something
                    Toast.makeText(this@ActionModeActivity, "Settings", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.action_search -> {
                    // Do something
                    Toast.makeText(this@ActionModeActivity, "Search", Toast.LENGTH_SHORT).show()
                    true
                }
                else -> false
            }
        }

        // Called when the user exits the action mode
        override fun onDestroyActionMode(mode: ActionMode?) {
            actionMode = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_action_mode)

        someTextView.setOnLongClickListener { v ->
            actionMode = startActionMode(mActionModeCallback);
            v.isSelected = true
            true
        }
    }
}

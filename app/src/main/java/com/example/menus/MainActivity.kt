package com.example.menus

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        actionBarButton.setOnClickListener {
            startActivity(Intent(this, ActionBarActivity::class.java))
        }
        toolbarButton.setOnClickListener {
            startActivity(Intent(this, ToolbarActivity::class.java))
        }
        contextMenuButton.setOnClickListener {
            startActivity(Intent(this, ContextMenuActivity::class.java))
        }
        popupMenuButton.setOnClickListener {
            startActivity(Intent(this, PopupMenuActivity::class.java))
        }
        actionModeButton.setOnClickListener {
            startActivity(Intent(this, ActionModeActivity::class.java))
        }
        navigationButton.setOnClickListener {
            startActivity(Intent(this, NavigationActivity::class.java))
        }
        bottomNavigationButton.setOnClickListener {
            startActivity(Intent(this, BottomNavigationActivity::class.java))
        }
    }
}

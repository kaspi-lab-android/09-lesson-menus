package com.example.menus

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_navigation_view.*

class NavigationActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_view)
        navigationView.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
            R.id.nav_gallery -> Toast.makeText(this, "Gallery", Toast.LENGTH_SHORT).show()
            R.id.nav_slideshow -> Toast.makeText(this, "Slideshow", Toast.LENGTH_SHORT).show()
            else -> return false
        }
        drawerLayout.closeDrawers()
        return true
    }
}

package com.example.menus

import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import kotlinx.android.synthetic.main.activity_popup_menu.*

class PopupMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_popup_menu)

        val popupMenu = PopupMenu(this, someButton)
        popupMenu.inflate(R.menu.menu_main)
        val popupHelper = MenuPopupHelper(
            this,
            popupMenu.menu as MenuBuilder,
            someButton,
            false,
            R.attr.popupMenuStyle,
            0
        )
        popupHelper.setForceShowIcon(true)
//        popupHelper.gravity = Gravity.TOP or Gravity.END
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_settings -> {
                    // Do something
                    Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show()
                }
                R.id.action_search -> {
                    // Do something
                    Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show()
                }
            }
            true
        }

        someButton.setOnClickListener {
            popupHelper.show()
        }
    }
}
